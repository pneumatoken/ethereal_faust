## Todo:
_"Da steh ich nun, ich armer Tor, und bin so klug als zuvor!"_  
do tutorial and contact [learnmeabitcoin.com](http://learnmeabitcoin.com/neo4j/how/) to import etherscan into Neo4j.  
# Ethereal Dr. Faust  

## Project summary:
A DAO or collection of solidty smart contracts between Mephisto and Dr. Faust, given perpetuity by having an ether lottery provide indefinate contract maintenance costs.  
_"Das also war des Pudels Kern!"_
## Meta contract or pseudo code  
### create sample contract between Dr. Faust's Ether address and Mephist's ether address that listens
"Die Botschaft hoer ich wohl, allein mir fehlt der Glaube"  
timed requests to multiple of Faust's "Panopticon TOR blockchain Oracles" of the Dark webs. After concensus of confirmation that Dr. Faust has comunicated (if)
encrypted passphrase _"Werd ich zum Augenblicke sagen: Verweile doch! du bist so schön!"_  (then) _"Dann magst du mich in Fesseln schlagen, Dann will ich gern zugrunde gehn!"_
### Purse string power; attach ethereum lottery to pay for perpetual contract maintenance 
_"Nach Golde draengt, am Golde haengt doch alles"_  
i.e. hosting and software upgrades optimization 
### [Aristagoras](https://en.wikipedia.org/wiki/Aristagoras) strategy; how to correct for mistakes and bad decisions 
_"Es irrt der Mensch, solang er strebt"_  
how to evaluate and correct for performance, draining of the jackpot
### Security issues
_"Heinrich! Mir graut's vor dir"_  
hacking the contract or the jackpot  
### Paul Samuelson's shoeleather costs calculator  
_"Zwar wiess ich viel, doch moecht ich alles wissen"_  
A solidity contract regularly reacting to a neo4j database by data scraping etherscan to find patterns of ethereum contract addresses with the highest ROI to adjust the contract.  
  
### [Pan Twardowski's spider](https://en.wikipedia.org/wiki/Pan_Twardowski)
_"Der Worte sind genug gewechselt, lasst mich auch entlich Taten sehn"_  

code algorithm "wicked" problem*; a way to signal "the current jackjpot is ???", and underlying probability mechanism to other contracts on the ethereum network,
and an optimization algoritm searching etherscan for contracts with optimal return on investment  
### [Allianca Luso-Britanica](https://en.wikipedia.org/wiki/Anglo-Portuguese_Alliance) Contract resolution and continuation  
_"Hier bin ich Mensch, hier darf ich's sein!"_  
Aftermath; contract renewal or admission of new contract participants


\* "wicked problem" one that could be clearly defined only by solving it, or by solving part of it - Horst Rittel and Melvin Webber
check out https://www.seedom.io/
## Table of Contents

* [Introduction (German)](#introduction)
* [Protips](#protips)
* [Support](#support)
* [Translations](#translations)
* [Contribution Guidelines](CONTRIBUTING.md)

# Introduction
"plus ca change plus a meme chose" 

Um wie der Cyrano de Bergerac etwas Panache zu haben. Habe ich als erstes projekt ein Goethe motiv genommen und codiere einen smart contract fuer Mephist und Dr. Faust. 

"Der Frueh geliebte, nicht mehr getruebte der khert zurueck" und naturlich "Das ewig weibliche zieht uns hinan" 

Die idee ist 
1. einen smart contract zu erstellen der ewig gilt oder bis der faust sagt "verweile doch! Du bist so schoen"  ein ethereum smarty contract mit einer fortlaufended ethereum lottery die als unbestewchliche vertragsuhr gilt!

2. Fuer PR zwecke einen kurzfilm zu machen eine moderne kurzfassung von Goethe Dr. Faust als programierer 
3. 
  
  
"Es ist nichts schrecklicher als eine taetige Unwissenheit"
  
  
<sup>[back to table of contents](#table-of-contents)</sup>
Open Source: Goethe would not have it any other way; "Es gibt keine patriotische Kunst und keine patriotische Wissenschaft. Beide gehoeren, wie alles hohe Gute der Ganzen Welt an..."
  
---

# Metamodernism a new interpredation of Faust as a Solidity Developer
## Goals  
inspired by faust's contract to solve the problem of non synchronized and non monotonic time accross nodes  
NEED
    * consistent timeline across replicas: consensus
    * ordering transactions across the system as well 
    * teh ordercorresponding to obsevered commit order  
      
* Idea develop a persistant solidity Lottery, which retains an ether fee to fund/and be a timer and assets that for other independent smart contracts to use, 
in peretuity. usecase: bonds or other financial securities with no fixed maturity date; legal inalienable estates beyond limits set by law.  
  
* Timekeeping mechanisms a imutable unidirectional force used to measure and sequence events. data values retived from a system depend on the consistancy model used. 
consistancey model set of guarantees the system makes about what events will be visible when. A set of vaild timelines enforced by the systems timekeeping mewchanisms. 
    * Computer clocks - system clock, NPT, Unix time other timekeeping mechanisms Spanner, Riak hardware clocks drift time now() = sys call clock_gettime(CLOCK_REALTIME) current UNIX timestamp. System clocks can not be synconized because hardware clocks increment at different rates resluting in drift. Network Time Protocol (NTP) a clock synconization protocol on the other hand syncronizes a system clock to a highly(er) accurate clock network, minimizing drift with atomic and GPS clocks. the protocol gradually adjusts either by "skewing" teh system clock interupt rate or by jumping the clock setting a new value "step" thereby reducing the time differential. Yet stepping creates discontinuous jumps or steps in time. the sytem clock maintains UNIX time or number of seconds since epoch - midnight UTC 01.01.1970. increasing exactly 86,400 seconds per day. UTC an inaccurate compromise between atomic time, the measured atomic decay adjusted to the continuously slowing astronomical time based on the rotation of the earth, by injecting a leap second just before midnight. to realine with UTC Unix time waits or repeats  a second at midnight therefore Unix time is not monotonic creating the problem of non synchronized and non monotonic time accross nodes. So regretable, newer versions of data will eventually reflect older timestamps accross increasingly unaligned nodes.  
    * the ned for other timekeeping mechanisms - higher consistancy lower reliabiloty tradeoff = desired availablity how responsive the mechanism is vs. desired performance read write latency and throughput  equals cost of higher consistency (CAP Theorem,etc.) 
    * Spanner = (1) Distributed relational database supporting distributed transactions (2) Horizontally scalable data; Savings data  partitioned from checking data (3) Geo-replicated for faul tolerance (4) Performant (5) Externally consistent = globally consistent ordering of transactions correponding to the externally observed commit order.
    
Paxos Raft concensus protocols synconisly replicate a consistant timeline across replicas  
order of transactions == observed order  
Spanner True Time = tracks and reveals teh uncertainty about perceived time across clocks.  
True time explicitly represents time as an interval not a point  
TT.now() = [earliest, latest] interval containg the "true now" = earliest is the earliest time that could be "true Now"; latest is the latest  
  
commit_ts(T1) = TT.now().latest  
waits for one full uncertainty window  = commit wait gurantees the next commit_ts of the next transaction  will have a higher later timestamp despite different clocks
i.e. until commit_ts < TT.now().earliest
  
TrueTime provides externally consistenjt transaction commit timestamps enabling exteranl consistency without coordination 
  
* Promotional screenplay and short film: modern day developer Faust making a Solidity contract with Software vendor Mephisto.  

[computer chess](http://www.metamodernism.com/2015/01/26/computer-chess-and-the-reclaiming-of-history-in-film/)  

#### [Demo](http://codepen.io/AllThingsSmitty/pen/kkrkLL)

**Note:** If you follow the [Inherit `box-sizing`](#inherit-box-sizing) tip
  
<sup>[back to table of contents](#table-of-contents)</sup>
![faust]("https://gitlab.com/pneumatoken/ethereal_faust/blob/master/site_images/8NAf.gif" width="400" alt="Solidity">)

![config IP addresses](https://github.com/charlesfinney/neo4j/blob/master/docs/site_images/Installing_Neo4j_on_Ubuntu_adding_IP_addresses.png)
